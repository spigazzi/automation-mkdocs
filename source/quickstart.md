# QuickStart guide

## Clone the repository

First duplicate or fork the Sample Automation repository in https://gitlab.cern.ch/cms-ppdweb/automation .
This will give you the structure of the repository to modify for your use-case.

!!! note
    It is strongly advised to keep the repository public.

## Obtain service account

To run the jobs on HTCondor you will need a Service Account with [robot certificate](https://ca.cern.ch/ca/Help/?kbid=021003) and added to the `zh: Linux group zh - CMS`.

Download the p12 file and copy it to the lxplus service account home directory. Then generate the private/public key pair using the commands in: https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookStartingGrid#ObtainingCert

Generate the keytab in the service account home directory.

## Create Jenkins admin account

Request Jenkins id for the project.

## Add credential to the repository

Add the service account credentials in the CI/CD Variables tab:

in you repository, go to **Settings > CI/CD > Variables**.
Add the Variables: 
 - SERVICE_USERNAME
 - SERVICE_PWD

## Build first docker image

Go to **Build > Pipelines > Run Pipeline**

Run the pipeline on master (default settings should be fine)
Make sure that the `build_docker_dev` passed, from the **Build > Jobs** tab.

## Unpack the docker image

From the **Deploy > Container Registry** tab, open the registry corresponding to your repository and search for `dev`. Click on the clipboard button next to the name to copy the full path.

Add the full path to https://gitlab.cern.ch/unpacked/sync/-/blob/master/recipe.yaml?ref_type=heads by opening a pull request to the [repository](https://gitlab.cern.ch/unpacked/sync).

In more or less half an hour your image will be unpacked in:
`/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/<YOUR-REPOSITORY-IMAGE>:dev`

## Configure repository
WIP

Update the `automationApptainerStep.groovy` in `automation/shared-lib/vars` adding the Jenkins id.


## Create custom workflow
WIP

## Create database
WIP

## Add campaign to database
WIP

## Assign workflows to campaign
WIP

## Create Jenkins Item
WIP

