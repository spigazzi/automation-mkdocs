# InfluxDB admin tasks

## Connect to the db
The influxdb pyhton client is used to perform admin tasks through a python interactive session.
Some of the low level actions can be performed using only the influxdb python package. Nonetheless, it is recommended to use the automation-control environment. 

```python
import influxdb

client = influxdb.InfluxDBClient(
            host="ecal-automation-relay.web.cern.ch",
            port=80,
            username="admin",
            password="ADMIN-PASSWORD",
            ssl=False,
            database="")
```

## Create new database

Create the database itself
```python
client.create_database('db_name')
```

Create new users if necessary. Usually separate between a READ only user and a READ/WRITE one.
```python
client.create_user('writeuser')
client.create_user('viewuser')
```

Grant privileges to the users:
```python
client.grant_privilege('READ', 'db_name', 'viewuser')
client.grant_privilege('ALL', 'db_name', 'writeuser')
```

??? Note 
    The influxdb users are completely independent of CERN accounts. One might choose to have the write user named as the service account used
    to access lxplus from Jenkins for convenience, but the two would still be independent.
    

## Manually set tasks status
Changing the status of tasks require using the RunCtrl class. In order to be able to write the new status the influxdb username and password should
be set to an account that as WRITE access to the db being used. This can be done setting the `ECAL_INFLUX_USER` and `ECAL_INFLUX_PWD` environment variables.

```python
from ecalautoctrl import RunCtrl

rctrl = RunCtrl(dbname="db_name", campaign="campaign_name")
rctrl.updateStatus(run=RUN_NUMBER, status={'task1-name':'done', 'task2-name' : 'done'})
```

The status of multiple tasks can be updated at once for a given run as shown above.
