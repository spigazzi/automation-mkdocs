# Introduction

This guide is meant to collect all the information related to the framework that 
provide automation of the CMS calibration and monitoring workflows.

The framework was originally designed and developed for ECAL calibration during the LHC LS2 and was later extended to all DPG and POG that want to profit from it.

The system has been designed following these principles:

- Rely as much as possible on market software, minimizing the need of ad-hoc code.
- Integrate the existing calibration workflows limiting the amount of code re-writing.
- Provide simple monitoring tools (here monitoring refers to the monitoring of the
  automation).
- Support both prompt-reco and re-recos calibration campaigns (ideally without the need to
  adjust the workflow too much when moving from one to the others).
  